import csv
import os
import argparse
import shutil
import datetime
import sys

def folderchek(pth, isJournal=False):
    if os.path.isdir(pth):
        return True
    else:
        try:
            os.makedirs(pth, mode=0o777, exist_ok=True)
            return True
        except PermissionError:
            if isJournal:
                print("Incorrect directory to journal, journal will be writed to the default folder")
            return False

def accesschek(pth):
    if os.access(pth, os.W_OK):
        return True
    else:
        print("Permission denied. Journal will be writed to the default folder")
        return False


def jounalwriter(out, time, status):
    flag = False

    if (args.j != 0) and (folderchek(args.j, True)) and (accesschek(args.j)):
        pth = os.path.join(args.j, "journal.csv")
    else:
        pth = os.path.join(os.path.abspath("."), "journal.csv")
    
    if os.path.isfile(pth):
        
        with open(pth, 'r') as read_obj:
            csv_reader = csv.DictReader(read_obj)
            header = next(csv_reader)
            if header != None:
                flag = True
            read_obj.close()
    with open(pth, mode='a') as csv_file:
        fieldnames = ['What backuped', 'Where backuped', 'When backuped', 'Status']
        writer = csv.DictWriter(csv_file, fieldnames=fieldnames)
        if not flag:
            writer.writeheader()
        writer.writerow({'What backuped': args.directory, 'Where backuped': out, 'When backuped': time, 'Status': status})
        csv_file.close()



parser = argparse.ArgumentParser()
parser.add_argument('--directory', action='store', required=True, help='Directory to backup')
parser.add_argument('--output', action='store', required=True, help='Backup output folder')
parser.add_argument('-a', choices=['zip', 'tar', 'gztar', 'bztar', 'xztar'], default='gztar', help='Archivate method')
parser.add_argument('-j', default=0, help='Target directory to journal file, default - currrent folder')

args = parser.parse_args()

now = datetime.datetime.utcnow().strftime(f"%Y-%m-%d_%H:%M:%S")

if os.path.isdir(args.directory):
    if (folderchek(args.output)) and (os.access(args.output, os.W_OK)):
        now = datetime.datetime.utcnow().strftime(f"%Y-%m-%d_%H:%M:%S")
        out = f"%(d)s_%(t)s" % {"d":args.directory, "t":now}
        my_archive = shutil.make_archive(out, args.a, args.directory)
        shutil.move(my_archive, args.output)
        jounalwriter(my_archive, now, "Succes")
        print(os.path.abspath(my_archive))
    else:
        jounalwriter("None", now, "Error")
        print("Output folder is not directory or permission denied")
else:
    jounalwriter("None", now, "Error")
    print("input folder is not directory")